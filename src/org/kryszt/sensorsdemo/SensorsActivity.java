package org.kryszt.sensorsdemo;

import java.util.ArrayList;
import java.util.List;

import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

public class SensorsActivity extends FragmentActivity implements
		SensorEventListener, OnClickListener, OnCheckedChangeListener {
	private static final String ORIENTATION_DIALOG = "orientation_dialog";
	private SensorManager sm;
	private Sensor lastSensor;
	private SensorView sensorView = null;
	private int lastDelay = SensorManager.SENSOR_DELAY_UI;
	private TextView txtStatus;
	private TextView txtFPS;
	private FPSCounter fpsCounter = new FPSCounter(20);

	private static final String FPS_FORMAT = "FPS = %2.2f";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		prepareObjects();
	}

	@Override
	protected void onResume() {
		super.onResume();
		updateOrientation();
		startSensor();
	}

	public void updateOrientation() {
		TextView tv = (TextView) findViewById(R.id.txt_orient);
		int orient = getRequestedOrientation();
		tv.setText(getText(R.string.orientation) + ": "
				+ DataHelpers.getNameForOrientation(orient));
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		Log.w(getClass().getCanonicalName(), "onConfigurationChanged");
	}

	@Override
	protected void onPause() {
		stopSensors();
		super.onPause();
	}

	private void prepareObjects() {
		sm = (SensorManager) getSystemService(SENSOR_SERVICE);
		txtStatus = (TextView) findViewById(R.id.txt_status);
		txtFPS = (TextView) findViewById(R.id.txt_fps);
		sensorView = (SensorView) findViewById(R.id.sensor_view);
		CheckBox cbScale = (CheckBox) findViewById(R.id.cb_same_scale);
		cbScale.setOnCheckedChangeListener(this);
		findViewById(R.id.btn_info).setOnClickListener(this);
		findViewById(R.id.btn_screen_orien).setOnClickListener(this);
		prepareSensorsSpinner();
		prepareDelaySpinner();
	}

	private void prepareDelaySpinner() {
		Spinner spinSpeed = (Spinner) findViewById(R.id.spinner_delay);
		spinSpeed.setAdapter(new ArrayAdapter<DataHelpers.NamedValue>(this,
				android.R.layout.simple_spinner_item, DataHelpers.delays()));
		spinSpeed.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				DataHelpers.NamedValue sdl = (DataHelpers.NamedValue) parent
						.getItemAtPosition(position);
				delaySelected(sdl);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
	}

	private void prepareSensorsSpinner() {
		List<Sensor> sensors = sm.getSensorList(Sensor.TYPE_ALL);
		List<SensorLabel> labels = new ArrayList<SensorLabel>(sensors.size());
		Log.i(getClass().getCanonicalName(), "Sensors: " + sensors.size());
		for (Sensor s : sensors) {
			Log.i(getClass().getCanonicalName(), "Sensor: " + s.getName());
			labels.add(new SensorLabel(s));
		}
		Spinner spinSensor = (Spinner) findViewById(R.id.spinner_sensor);
		spinSensor.setAdapter(new ArrayAdapter<SensorLabel>(this,
				android.R.layout.simple_spinner_item, labels));
		spinSensor.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				SensorLabel label = (SensorLabel) parent
						.getItemAtPosition(position);
				sensorSelected(label.sensor);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				sensorSelected(null);
			}
		});

	}

	private void startSensor() {
		if (lastSensor != null) {
			sm.registerListener(this, lastSensor, lastDelay);
		}
	}

	void sensorSelected(Sensor sensor) {
		if (lastSensor != null) {
			sm.unregisterListener(this, lastSensor);
		}
		sensorView.reset();
		fpsCounter.clear();
		txtStatus.setText(R.string.waiting_for_reading);
		lastSensor = sensor;
		if (lastSensor != null) {
			sm.registerListener(this, lastSensor, lastDelay);
		}
	}

	void delaySelected(DataHelpers.NamedValue sdl) {
		lastDelay = sdl.value;
		if (lastSensor != null) {
			sm.unregisterListener(this, lastSensor);
			sm.registerListener(this, lastSensor, lastDelay);
		}
	}

	protected void stopSensors() {
		if (sm != null) {
			sm.unregisterListener(this);
		}
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (!event.sensor.equals(lastSensor)) {
			return;
		}
		float fps = fpsCounter.update();
		sensorView.updateSensor(event.values);
		String accur = DataHelpers.accuracyString(event.accuracy);
		txtStatus.setText(getText(R.string.accuracy) + ": " + accur);
		txtFPS.setText(String.format(FPS_FORMAT, fps));
	}

	private void showSensorInfo() {
		FragmentManager fm = getSupportFragmentManager();
		SensorDialogFragment sdf = new SensorDialogFragment();
		sdf.setSensor(lastSensor);
		sdf.show(fm, "sensor_dialog");
	}

	private void showOrientationDialog() {
		OrientationDialog od = new OrientationDialog();
		od.show(getSupportFragmentManager(), ORIENTATION_DIALOG);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_info) {
			showSensorInfo();
		} else if (v.getId() == R.id.btn_screen_orien) {
			showOrientationDialog();
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (buttonView.getId() == R.id.cb_same_scale) {
			sensorView.setCommonScales(isChecked);
		}
	}

	private static class SensorLabel {
		private final Sensor sensor;

		public SensorLabel(Sensor sensor) {
			super();
			this.sensor = sensor;
		}

		@Override
		public String toString() {
			return sensor.getName();
		}
	}
}